-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 14, 2019 at 04:12 PM
-- Server version: 5.7.9
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myfarm`
--

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

DROP TABLE IF EXISTS `block`;
CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `description` text,
  `other` text,
  `url` varchar(255) DEFAULT NULL,
  `icon` smallint(6) DEFAULT NULL,
  `order` smallint(11) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`id`, `title`, `category`, `description`, `other`, `url`, `icon`, `order`, `slug`, `status`) VALUES
(1, 'High Security', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 6, NULL, 'high-security', 1),
(2, 'Worldwide Support', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 2, NULL, 'worldwide-support', 1),
(3, 'User Friendly', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 5, NULL, 'user-friendly', 1),
(4, 'Better Performance', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 3, NULL, 'better-performance', 1),
(5, 'Attractive Design', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 4, NULL, 'attractive-design', 1),
(6, 'Device Compatibility', NULL, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est high..</p>', '<ul>\r\n<li>Lorem ipsum dolor sit amet, consectetur adipiscing</li>\r\n<li>tempor incididunt ut labore et dolore magna aliqua</li>\r\n</ul>', NULL, 1, NULL, 'device-compatibility', 1);

-- --------------------------------------------------------

--
-- Table structure for table `block_image`
--

DROP TABLE IF EXISTS `block_image`;
CREATE TABLE IF NOT EXISTS `block_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `b_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `imagemanager`
--

DROP TABLE IF EXISTS `imagemanager`;
CREATE TABLE IF NOT EXISTS `imagemanager` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fileName` varchar(128) NOT NULL,
  `fileHash` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagemanager`
--

INSERT INTO `imagemanager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(1, 'features1.png', 'GC0XRJIWVvoENUnYsKOzrHk-6388df7z', '2019-08-06 18:44:00', '2019-08-06 18:44:00', NULL, NULL),
(2, 'features2.png', 'AUck2rQu_rdB-EtF3Vwqw-xaz9gzd7O2', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(3, 'features3.png', 'yyMSkZrLVOtcagi0vwBwT1NEZ0yd6u3h', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(4, 'features4.png', 'q0ogFyYI-NcbeZ7chtS6Ipugf4at9arA', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(5, 'features5.png', 'bX8noZR_LzjYCwbSoJs0N0zSZ4IgMVCY', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(6, 'features6.png', '5VgL5SyS5_Ze2O0koberhFjUFagbh7_5', '2019-08-06 18:44:02', '2019-08-06 18:44:02', NULL, NULL),
(7, 'device-xs-1.png', 't3fz9bE2QokPJPRk26OKWjdunMy_u3XA', '2019-08-09 22:14:49', '2019-08-09 22:14:49', NULL, NULL),
(8, 'device-xs-2.png', 'X97XRXMT-4JRFc3jaFRZ1UBUQgBHhCqU', '2019-08-09 22:14:53', '2019-08-09 22:14:53', NULL, NULL),
(9, 'logo.png', 'diVIV9hPFOI9RbwDOlJcriVbg5jMf4um', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(10, 'logo-all.png', '5bm4nP6yZdAGUSA--gPh59gYKwc8KyKv', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(11, 'youtube-thumb.png', 'kTzo52H5xNapuYRt9w4eTXLNwTw_7FOs', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(12, 'mission.png', 'qgPNzPr0gAgClux6fMpXYfS2lBCgzah7', '2019-08-14 11:33:48', '2019-08-14 11:33:48', NULL, NULL),
(13, 'vision.png', 'DqYyVgZYevcOiNP-2XZySWe9SDOsA_L2', '2019-08-14 11:33:53', '2019-08-14 11:33:53', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`,`language`),
  KEY `idx_message_language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'en-US', 'Sed ut perspiciatis unde omnis iste natus error sit'),
(3, 'en-US', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quist'),
(4, 'en-US', 'HOW IT WORKS?'),
(5, 'en-US', 'App Features'),
(6, 'en-US', 'Lorem ipsum dolor sit amet consectetur'),
(7, 'en-US', 'What People are saying?'),
(8, 'en-US', '© {0,date,Y} Copyright My Fam Trip . All Rights Reserved.'),
(9, 'en-US', 'Feel Free To Drop A Message'),
(10, 'en-US', 'We will contact you within one business day.'),
(11, 'en-US', 'Thank you for contacting us. We will respond to you as soon as possible.'),
(12, 'en-US', 'There was an error sending your message.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1564485438),
('m130524_201442_init', 1564485446),
('m190124_110200_add_verification_token_column_to_user_table', 1564485447),
('m150207_210500_i18n_init', 1564818055),
('m160622_085710_create_ImageManager_table', 1565096103),
('m170223_113221_addBlameableBehavior', 1565096103);

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

DROP TABLE IF EXISTS `node`;
CREATE TABLE IF NOT EXISTS `node` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `description` text,
  `other` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `setting` tinyint(1) DEFAULT '0',
  `digit1` decimal(10,2) DEFAULT NULL,
  `digit2` decimal(10,2) DEFAULT NULL,
  `digit3` decimal(10,2) DEFAULT NULL,
  `digit4` decimal(10,2) DEFAULT NULL,
  `digit5` decimal(10,2) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `text1` text,
  `text2` text,
  `text3` text,
  `text4` text,
  `text5` text,
  `started_at` int(11) DEFAULT NULL,
  `ended_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `base` tinyint(1) DEFAULT NULL,
  `menu` tinyint(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  KEY `updated_by` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node`
--

INSERT INTO `node` (`id`, `title`, `summary`, `description`, `other`, `image`, `video`, `price`, `link`, `file`, `setting`, `digit1`, `digit2`, `digit3`, `digit4`, `digit5`, `field1`, `field2`, `field3`, `field4`, `field5`, `text1`, `text2`, `text3`, `text4`, `text5`, `started_at`, `ended_at`, `created_at`, `updated_at`, `created_by`, `updated_by`, `status`, `base`, `menu`, `type`, `slug`) VALUES
(3, 'About The App', 'Lorem ipsum dolor sit amet consectetur', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>', '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565287006, 1565360206, 1, 1, 1, 1, NULL, 'advanced', 'about-the-app'),
(4, 'Contact', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565414271, 1565414271, 1, 1, 1, 1, NULL, 'basic', 'contact'),
(5, 'Privacy Policy', 'Lorem ipsum dolor sit amet consectetur', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565455706, 1565455706, 1, 1, 1, 1, NULL, 'basic', 'privacy-policy'),
(6, 'John Doe', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et', NULL, 'John-Doe-1565515308.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Ceo', 'Loremipsum', NULL, NULL, NULL, NULL, NULL, 1565515193, 1565517016, 1, 1, 1, NULL, NULL, 'testimonial', 'john-doe'),
(7, 'About Us', 'Lorem ipsum dolor sit amet consectetur', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>', '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565694686, 1565694686, 1, 1, 1, 1, NULL, 'advanced', 'about-us'),
(9, 'mobile screen 1', NULL, NULL, NULL, 'mobile-screen-1-1565702843.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565702843, 1565702843, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-1'),
(10, 'mobile screen 2', NULL, NULL, NULL, 'mobile-screen-2-1565702932.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565702932, 1565702932, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-2'),
(11, 'mobile screen 3', NULL, NULL, NULL, 'mobile-screen-3-1565702950.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565702950, 1565702950, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-3');

-- --------------------------------------------------------

--
-- Table structure for table `node_image`
--

DROP TABLE IF EXISTS `node_image`;
CREATE TABLE IF NOT EXISTS `node_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) DEFAULT NULL,
  `node_image` varchar(255) DEFAULT NULL,
  `node_image_title` varchar(255) DEFAULT NULL,
  `node_image_description` text,
  `default` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `article_id` (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_image`
--

INSERT INTO `node_image` (`id`, `nid`, `node_image`, `node_image_title`, `node_image_description`, `default`) VALUES
(3, 3, 'image1-1565287006.png', 'image1', 'image1', 0),
(4, 3, 'image2-1565360206.png', 'image2', 'image2', 0),
(5, 3, 'image3-1565360207.png', 'image3', 'image3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `node_sub`
--

DROP TABLE IF EXISTS `node_sub`;
CREATE TABLE IF NOT EXISTS `node_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nid` int(11) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `icon` int(11) DEFAULT NULL,
  `sub_description` text,
  PRIMARY KEY (`id`),
  KEY `fk_sub_node` (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_sub`
--

INSERT INTO `node_sub` (`id`, `nid`, `sub_title`, `icon`, `sub_description`) VALUES
(5, 7, 'Mission', 12, '<p>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea</p>'),
(6, 7, 'Vision', 13, '<p>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea</p>');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(510) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `module`, `name`, `value`) VALUES
(1, 'Image Upload Location', 'site', 'path', '../uploads'),
(2, 'Image Upload Url', 'site', 'url', 'http://localhost/mft/uploads/'),
(3, 'Site name', 'base', 'name', 'MFT'),
(4, 'CSS file location', 'site', 'css', '../webassets/css/'),
(5, 'JS file location', 'site', 'js', '../webassets/js/'),
(6, 'Template Path', 'site', 'template', '@frontend/views/template/'),
(7, 'Layout Path', 'site', 'layout', '@frontend/views/layouts/'),
(8, 'Widget Path', 'site', 'widget', '@frontend/views/widgets/'),
(9, 'Block path', 'site', 'block', '@frontend/views/blocks/'),
(10, 'SMTP Host', 'smtp', 'host', 'smtp.mailtrap.io'),
(11, 'SMTP Username', 'smtp', 'username', 'cac91bdf122d99'),
(12, 'SMTP Password', 'smtp', 'password', '28f769256857be'),
(13, 'SMTP Port', 'smtp', 'port', '2525'),
(14, 'SMTP Encryption', 'smtp', 'encryption', 'tls'),
(15, 'Facebook', 'custom', 'facebook', ''),
(16, 'Twitter', 'custom', 'twitter', ''),
(17, 'Instagram', 'custom', 'instagram', ''),
(18, 'Youtube Link', 'custom', 'youtube-link', 'https://youtu.be/8042wzYZ5fY'),
(19, 'Android Link', 'custom', 'android-link', ''),
(20, 'Ios Link', 'custom', 'ios-link', ''),
(21, 'Logo Header', 'img', 'logo-header', '10'),
(22, 'Logo Body', 'img', 'logo-body', '9'),
(23, 'Youtube Image', 'img', 'youtube-image', '11'),
(24, 'Google Map Key', 'custom', 'google-map-key', 'AIzaSyDNlOALy3golRPLA8Fmwz25FuL-kmPEF-Q'),
(25, 'SMTP Email', 'smtp', 'email', 'vinod-560b68@inbox.mailtrap.io'),
(26, 'email', 'custom', 'email', 'info@myfamtrip.com'),
(27, 'Captcha Site Key', 'custom', 'captcha-site-key', '6Le47rIUAAAAAA2tO0tFhSh2SbZNuTS42S9DFQY9'),
(28, 'Captcha Secret Key', 'custom', 'captcha-secret-key', '6Le47rIUAAAAAHgtd710BtlkgTQqmJlSnxJCo_Nj');

-- --------------------------------------------------------

--
-- Table structure for table `source_message`
--

DROP TABLE IF EXISTS `source_message`;
CREATE TABLE IF NOT EXISTS `source_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idx_source_message_category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, '*', 'header_banner_title'),
(3, '*', 'header_banner_description'),
(4, '*', 'header_how_it'),
(5, '*', 'feature_title'),
(6, '*', 'feature_summary'),
(7, '*', 'what_people'),
(8, '*', 'footer_copyright'),
(9, '*', 'contact_sub_title'),
(10, '*', 'contact_note'),
(11, '*', 'contact_email_success'),
(12, '*', 'contact_email_error');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
CREATE TABLE IF NOT EXISTS `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `style` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `style`, `type`, `file`) VALUES
(1, 'list', 'basic', 'listing-page.php'),
(2, 'list', 'service', 'listing-page.php'),
(3, 'list', 'package', 'package-listing.php'),
(4, 'detail', 'package', 'details-page.php'),
(5, 'detail', 'package', 'package-details.php'),
(6, 'detail', 'category', 'details-page.php'),
(7, 'detail', 'service', 'details-page.php'),
(8, 'detail', 'advanced', 'details-page.php'),
(9, 'detail', 'basic', 'details-page.php'),
(10, 'list', 'advanced', 'listing-page.php');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'W_4ZWWcpQZQ-wc5LuLwQ6UPasB8VZs_U', '$2y$13$pL7EZ3LR7ghLcC/Vb8UDAufOOlwefgRqPDRGKceKs/05SIdpxVwTG', NULL, 'admin@admin.com', 10, 1564485595, 1564485595, '0C7B0Hk63uCB-27o6T9_sP9PgZfvq75G_1564485595');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `block_image`
--
ALTER TABLE `block_image`
  ADD CONSTRAINT `fk_block_images` FOREIGN KEY (`b_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `node_image`
--
ALTER TABLE `node_image`
  ADD CONSTRAINT `fk_node_image` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `node_sub`
--
ALTER TABLE `node_sub`
  ADD CONSTRAINT `fk_sub_node` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
