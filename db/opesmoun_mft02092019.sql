-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 02, 2019 at 03:26 PM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opesmoun_mft`
--

-- --------------------------------------------------------

--
-- Table structure for table `block`
--

CREATE TABLE `block` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `description` text,
  `other` text,
  `url` varchar(255) DEFAULT NULL,
  `icon` smallint(6) DEFAULT NULL,
  `order` smallint(11) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block`
--

INSERT INTO `block` (`id`, `title`, `category`, `description`, `other`, `url`, `icon`, `order`, `slug`, `status`) VALUES
(1, 'CHOOSE THE PLAN THAT’S RIGHT FOR YOU: ', NULL, '<p>Maybe you&rsquo;re just looking for Fam tips and pointers? Maybe you&rsquo;re ready to up your Fam game &amp; receive a return on your investment?</p>\r\n<p>Free App Download: Gives you access to...</p>\r\n<ul>\r\n<li>Marketing tips</li>\r\n<li>Fam tips</li>\r\n<li>Fam etiquette</li>\r\n</ul>\r\n<p>Upgrade to Subscription to Make the most of your Fam experience!</p>', '<ul>\r\n<li>A systematic way to observe and record your Fams</li>\r\n<li>Capture all the details of your Fam in one place</li>\r\n<li>Record all pertinent information (static and subjective)</li>\r\n<li>Store all Fam trip data</li>\r\n<li>Auto generate Fam reports (to keep or share!)</li>\r\n<li>Know when to capture the Fam money shots!</li>\r\n<li>Market your Fam experience on the go!</li>\r\n<li>Send client messages about a resort that is perfect for them!</li>\r\n<li>Auto watermark all Fam photos</li>\r\n</ul>', NULL, 5, NULL, 'choose-the-plan-thats-right-for-you', 1),
(2, ' ATTRACTIVE DESIGN', NULL, '<ul>\r\n<li>Option to select Site Inspection, Cruise or Guided Tour</li>\r\n<li>Easily bounce between categories (beach &amp; restaurants or Spa &amp; Pools, etc.)</li>\r\n<li>Quickly pull your historic Fam trips to view &amp; share</li>\r\n</ul>', '', NULL, 4, NULL, 'attractive-design-2', 1),
(3, 'USER FRIENDLY', NULL, '<p>Everything you need in the palm of your hand!</p>', '', NULL, 1, NULL, 'user-friendly', 1),
(4, 'DEVICE COMPATIBILITY', NULL, '<ul>\r\n<li>IPhone 6</li>\r\n<li>Iphone 7</li>\r\n<li>Iphone 8</li>\r\n<li>Android version 9</li>\r\n</ul>', '', NULL, 3, NULL, 'device-compatibility-2', 1),
(5, 'Worldwide Support', NULL, '', '', NULL, 2, NULL, 'worldwide-support', 0),
(6, 'High Security', NULL, '', '', NULL, 6, NULL, 'high-security', 0);

-- --------------------------------------------------------

--
-- Table structure for table `block_image`
--

CREATE TABLE `block_image` (
  `id` int(11) NOT NULL,
  `b_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ImageManager`
--

CREATE TABLE `ImageManager` (
  `id` int(10) UNSIGNED NOT NULL,
  `fileName` varchar(128) NOT NULL,
  `fileHash` varchar(32) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `createdBy` int(10) UNSIGNED DEFAULT NULL,
  `modifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ImageManager`
--

INSERT INTO `ImageManager` (`id`, `fileName`, `fileHash`, `created`, `modified`, `createdBy`, `modifiedBy`) VALUES
(1, 'features1.png', 'GC0XRJIWVvoENUnYsKOzrHk-6388df7z', '2019-08-06 18:44:00', '2019-08-06 18:44:00', NULL, NULL),
(2, 'features2.png', 'AUck2rQu_rdB-EtF3Vwqw-xaz9gzd7O2', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(3, 'features3.png', 'yyMSkZrLVOtcagi0vwBwT1NEZ0yd6u3h', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(4, 'features4.png', 'q0ogFyYI-NcbeZ7chtS6Ipugf4at9arA', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(5, 'features5.png', 'bX8noZR_LzjYCwbSoJs0N0zSZ4IgMVCY', '2019-08-06 18:44:01', '2019-08-06 18:44:01', NULL, NULL),
(6, 'features6.png', '5VgL5SyS5_Ze2O0koberhFjUFagbh7_5', '2019-08-06 18:44:02', '2019-08-06 18:44:02', NULL, NULL),
(7, 'device-xs-1.png', 't3fz9bE2QokPJPRk26OKWjdunMy_u3XA', '2019-08-09 22:14:49', '2019-08-09 22:14:49', NULL, NULL),
(8, 'device-xs-2.png', 'X97XRXMT-4JRFc3jaFRZ1UBUQgBHhCqU', '2019-08-09 22:14:53', '2019-08-09 22:14:53', NULL, NULL),
(9, 'logo.png', 'diVIV9hPFOI9RbwDOlJcriVbg5jMf4um', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(10, 'logo-all.png', '5bm4nP6yZdAGUSA--gPh59gYKwc8KyKv', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(11, 'youtube-thumb.png', 'kTzo52H5xNapuYRt9w4eTXLNwTw_7FOs', '2019-08-09 22:22:21', '2019-08-09 22:22:21', NULL, NULL),
(12, 'mission.png', 'qgPNzPr0gAgClux6fMpXYfS2lBCgzah7', '2019-08-14 11:33:48', '2019-08-14 11:33:48', NULL, NULL),
(13, 'vision.png', 'DqYyVgZYevcOiNP-2XZySWe9SDOsA_L2', '2019-08-14 11:33:53', '2019-08-14 11:33:53', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(4, 'en-US', 'HOW IT WORKS?'),
(5, 'en-US', 'App Features'),
(7, 'en-US', 'What People are saying?'),
(8, 'en-US', '© {0,date,Y} Copyright My Fam Trip . All Rights Reserved.'),
(9, 'en-US', 'Feel Free To Drop A Message'),
(10, 'en-US', 'We will contact you within one business day.'),
(11, 'en-US', 'Thank you for contacting us. We will respond to you as soon as possible.'),
(12, 'en-US', 'There was an error sending your message.\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1564485438),
('m130524_201442_init', 1564485446),
('m190124_110200_add_verification_token_column_to_user_table', 1564485447),
('m150207_210500_i18n_init', 1564818055),
('m160622_085710_create_ImageManager_table', 1565096103),
('m170223_113221_addBlameableBehavior', 1565096103);

-- --------------------------------------------------------

--
-- Table structure for table `node`
--

CREATE TABLE `node` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `summary` text,
  `description` text,
  `other` text,
  `image` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `setting` tinyint(1) DEFAULT '0',
  `digit1` decimal(10,2) DEFAULT NULL,
  `digit2` decimal(10,2) DEFAULT NULL,
  `digit3` decimal(10,2) DEFAULT NULL,
  `digit4` decimal(10,2) DEFAULT NULL,
  `digit5` decimal(10,2) DEFAULT NULL,
  `field1` varchar(255) DEFAULT NULL,
  `field2` varchar(255) DEFAULT NULL,
  `field3` varchar(255) DEFAULT NULL,
  `field4` varchar(255) DEFAULT NULL,
  `field5` varchar(255) DEFAULT NULL,
  `text1` text,
  `text2` text,
  `text3` text,
  `text4` text,
  `text5` text,
  `started_at` int(11) DEFAULT NULL,
  `ended_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `base` tinyint(1) DEFAULT NULL,
  `menu` tinyint(4) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node`
--

INSERT INTO `node` (`id`, `title`, `summary`, `description`, `other`, `image`, `video`, `price`, `link`, `file`, `setting`, `digit1`, `digit2`, `digit3`, `digit4`, `digit5`, `field1`, `field2`, `field3`, `field4`, `field5`, `text1`, `text2`, `text3`, `text4`, `text5`, `started_at`, `ended_at`, `created_at`, `updated_at`, `created_by`, `updated_by`, `status`, `base`, `menu`, `type`, `slug`) VALUES
(3, 'About The App', 'Lorem ipsum dolor sit amet consectetur', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>', '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565287006, 1565360206, 1, 1, 1, 1, NULL, 'advanced', 'about-the-app'),
(4, 'Contact', '', '', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565414271, 1565414271, 1, 1, 1, 1, NULL, 'basic', 'contact'),
(5, 'Privacy Policy', 'Last modified: August 1, 2019', '<p><u>Introduction</u></p>\r\n<p>My Fam Trip, LLC (<strong>\"My Fam Trip\"</strong> or<strong> \"We\"</strong>) respect your privacy and are committed to protecting it through our compliance with this policy.</p>\r\n<p>This policy describes the types of information we may collect from you or that you may provide when you visit the website <a href=\"http://www.myfamtrip.com\">www.myfamtrip.com</a> (our \"<strong>Website</strong>\") and our practices for collecting, using, maintaining, protecting, and disclosing that information.</p>\r\n<p>This policy applies to information on this Website.</p>\r\n<p>It does not apply to information collected by us offline or through any other means, including on any other website operated by My Fam Trip or any third party, including through any application or content (including advertising) that may link to or be accessible from the Website.</p>\r\n<p>Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is to not use our Website. By accessing or using this Website, you agree to this privacy policy. This policy may change from time to time (see <u>Changes to Our Privacy Policy</u>). Your continued use of this Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</p>\r\n<p><u>We Do Not Collect Information About You</u></p>\r\n<p>We do not collect any information from and about users of our Website.</p>\r\n<p>We do not collect personal information automatically. However, we may obtain personal information about you that we collect from other sources or you provide to us.</p>\r\n<p><u>Third-Party Use of Cookies and Other Tracking Technologies</u></p>\r\n<p>Some content or applications, including advertisements, on the Website are served by third-parties, including advertisers, ad networks and servers, content providers, and application providers. These third parties may use cookies alone or in conjunction with web beacons or other tracking technologies to collect information about you when you use our website. The information they collect may be associated with your personal information or they may collect information, including personal information, about your online activities over time and across different websites and other online services. They may use this information to provide you with interest-based (behavioral) advertising or other targeted content.</p>\r\n<p>We do not control these third parties\' tracking technologies or how they may be used. If you have any questions about an advertisement or other targeted content, you should contact the responsible provider directly.</p>\r\n<p><u>Changes to Our Privacy Policy</u></p>\r\n<p>It is our policy to post any changes we make to our privacy policy on this page with a notice that the privacy policy has been updated on the Website home page. If we make material changes to how we treat our users\' personal information, we will notify you through a notice on the Website home page. The date the privacy policy was last revised is identified at the top of the page. You are responsible for periodically visiting our Website and this privacy policy to check for any changes.</p>\r\n<p><u>Contact Information</u></p>\r\n<p>To ask questions or comment about this privacy policy and our privacy practices, contact us at:&nbsp; hello@myfamtrip.com</p>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565455706, 1566935804, 1, 1, 1, 1, NULL, 'basic', 'privacy-policy'),
(7, 'About Us', 'Lorem ipsum dolor sit amet consectetur', '<p>Once upon a time, in 2017, Rochelle &amp; Stephanie had a nightmare. There were 15 travel advisors on a fam. Every advisor was recording their experience through multiple methods. Many advisors were not able to market their Fam experience to current and prospective clients. After a long day of sites, many didn&rsquo;t remember which resort was which. It was pure madness! The fam host was frustrated, having to repeat herself numerous times because some advisors were taking videos/pics, sitting on the beds, grabbing a beverage from the mini bar, and asking how much the suite sells for! They woke up screaming when an advisor slipped into the bathroom to use the facilities! They looked at each other with terror in their eyes and simultaneously said &ldquo;There has to be a better way!&rdquo; Stephanie and Rochelle immediately headed to a resort with pen and paper in hand to draft a tool which would make advisor&rsquo;s lives easier. Eventually, My Fam Trip was born. All who utilized the app across the land lived happily ever after! ...but seriously... My Fam Trip (&ldquo;MFT&rdquo;) was a passion project for us since 2017. With over 28 collective years in the travel industry, we observed the very unsystematic way in which Advisors experience Fams. We wanted to create a tool that would be efficient and monetarily beneficial to those in this profession we adore.</p>', '', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565694686, 1566935245, 1, 1, 1, 1, NULL, 'advanced', 'about-us'),
(9, 'mobile screen 1', NULL, NULL, NULL, 'mobile-screen-1-1565702843.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565702843, 1565702843, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-1'),
(10, 'mobile screen 2', NULL, NULL, NULL, 'mobile-screen-2-1565702932.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1565702932, 1565702932, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-2'),
(12, 'Mary', NULL, 'Excellent ', NULL, 'John-1566935081.jpg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'CEO', 'Test Company', NULL, NULL, NULL, NULL, NULL, 1566935081, 1566935091, 1, 1, 1, NULL, NULL, 'testimonial', 'mary'),
(13, 'mobile screen 3', NULL, NULL, NULL, 'mobile-screen-3-1567411074.png', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1567411074, 1567411074, 1, 1, 1, NULL, NULL, 'banner', 'mobile-screen-3'),
(14, 'Terms & Conditions', '', '<p>Your use of this My Fam Trip, LLC (&ldquo;My Fam&rdquo;) website (&ldquo;Site&rdquo;) signifies your agreement to these terms and conditions (\"Terms\").&nbsp; If you do not agree to these Terms, please do not use this Site.&nbsp; My Fam Trip reserves the right to make changes and corrections at any time, without notice, to this Site.&nbsp;</p>\r\n<h3>Limitation on Use</h3>\r\n<p>The content and information on this Site is proprietary to My Fam Trip. As a visitor to this Site, you agree not to use this Site or its contents or information for any commercial or non-personal purpose.&nbsp; In addition, whether or not you have a commercial purpose, you agree not to:</p>\r\n<p>(i) access, monitor or copy any content from this Site using a robot, spider, scraper or other automated means or any manual process for any purpose;</p>\r\n<p>(ii) violate the restrictions in robot exclusion headers on this Site or bypass or circumvent other measures employed to prevent or limit access to this Site;</p>\r\n<p>(iii) use any device, software or routine that interferes or attempts to interfere with the normal operation of Site take any action that, in My Fam Trip&rsquo;s sole discretion, imposes an unreasonable or disproportionately large burden on the Site;</p>\r\n<p>(iv) deep-link to any portion of this Site (including, without limitation, the purchase path for any travel services);</p>\r\n<p>(v) reproduce, duplicate, copy, sell, trade, resell or exploit Site;</p>\r\n<p>(vi) use any feature of Site for any purpose that is unlawful, harmful, or otherwise objectionable or inappropriate, as determined by us;</p>\r\n<p>(vii) post or distribute any material on Site that violates the rights of any third party or applicable law;</p>\r\n<p>(viii) use Site to collect or store personal data about others;</p>\r\n<p>(ix) use Site for any commercial purpose; or</p>\r\n<p>(x) transmit any ad or promotional materials on Site.</p>\r\n<h3>Limitation of Liability, No Representations</h3>\r\n<p>My Fam Trip expressly disclaims all liability for the use, reliability, or specific features of this Site, or the content or interpretation of content contained in this Site. If you use or make decisions based on information contained on this Site, you do so at your own risk. In exchange for using this Site, you agree to hold My Fam Trip harmless against any claims for damages arising from any decisions that you may make based on such information or any use that you make based on such information contained on this Site. While My Fam Trip has tried to provide accurate and timely information, this Site may contain inadvertent technical or factual inaccuracies and typographical errors. My Fam Trip does not represent or warrant that the information accessible via this site is accurate, complete or current.</p>\r\n<p>Information on this web site is provided on an \"as is\" and \"as available\" basis, without warranty of any kind, either express or implied, including, but not limited to, the implied warranties of merchantability, fitness for a particular purpose, or non-infringement. under no circumstances, including negligence, shall My Fam Trip be liable for any direct, indirect, incidental, special, punitive, or consequential damages that result from the use of or inability to use this site, nor shall My Fam Trip be responsible for any damages whatsoever that result from mistakes, omissions, interruptions, deletion of files, errors, defects, delays in operation or transmission, or any failure of performance whether or not caused by events beyond My Fam Trip&rsquo;s reasonable control, including but not limited to acts of god, communications line failure, theft, destruction, or unauthorized access to this site\'s records, programs, or services. in no event shall our total liability for all damages, losses, and causes of action exceed one dollar (us $1.00). Some jurisdictions do not allow the exclusion of implied warranties or the exclusion or limitations of certain damages, so the above exclusion may not apply to you.&nbsp;</p>\r\n<p>The airlines, hotels and other suppliers whose travel or other services are found on this site are independent contractors, not agents or employees of My Fam Trip.&nbsp; My Fam Trip is not liable for the acts, errors, omissions, representations, warranties, breaches, or negligence of these suppliers, or for any personal injuries, death, property damage, or other damages or expenses resulting therefrom. My Fam Trip has no liability for, and will make no refund in the event of any delay, cancellation, overbooking, strike, force majeure or other causes beyond its direct control.&nbsp; My Fam Trip takes no responsibility for any additional expense, omissions, delays, re-routing or acts of any government or authority. By offering for sale travel to particular international destinations, My Fam Trip does not represent or warrant that such travel is without risk.&nbsp;</p>\r\n<p>International Travel: You should read government-issued travel prohibitions, warnings, announcements and advisories before booking international travel.&nbsp; This information can be obtained from www.state.gov, www.tsa.gov, www.dot.gov, www.faa.gov, www.cdc.gov, www.treas.gov/ofac and www.customs.gov.</p>\r\n<h3>Promotions and Pricing&nbsp;</h3>\r\n<p>Price and availability of all travel and other services offered on this Site is subject to change without notice.&nbsp; Tax on hotel and car transactions is a recovery of all applicable transaction taxes (e.g. sales and use, occupancy, room tax, excise tax, value added tax, etc.) that My Fam Trip pays to suppliers in connection with your travel arrangements.&nbsp; Taxes vary by location and type of service provided.&nbsp; My Fam Trip is not a co-vendor, and is not the entity that collects and remits taxes to the tax authorities.</p>\r\n<p>The information on this Site may contain typographical errors or inaccuracies and may not be complete or current. My Fam Trip therefore reserves the right to correct any errors, inaccuracies or omissions and to change or update information at any time without prior notice. Please note that such errors, inaccuracies or omissions may relate to product/service description, pricing and availability.&nbsp;</p>\r\n<h3>Intellectual Property</h3>\r\n<p>My Fam Trip and other licensors, own all of the text, images, trademarks, service marks and other material contained on the Site. You may not copy or transmit any of the material except if you are doing so for your personal, non-commercial use. All copyright, trademark and other proprietary rights notices presented on the Site must appear on all copies you print. Other non-My Fam Trip product, service, or company designations on the Site belong to those respective third parties and may be mentioned in the Site for identification purposes only. You should contact the appropriate third party for more complete information regarding such designations and their registration status. Your use of and access to the Site does not grant you any license or right to use any of the marks included on the Site.</p>\r\n<h3>Third Party Links</h3>\r\n<p>This site may contain links to web sites and servers maintained by other persons or organizations.&nbsp; These links are provided as a service to users and are not sponsored by or affiliated with us.&nbsp; Information you submit at a third party site accessible from this Site is subject to the terms of that site\'s privacy policy, and we have no control over how your information is collected, used, or otherwise handled.&nbsp; We do not provide any warranty about the accuracy or source of information found on any of these third party sites or servers, or the content of any file the user might choose to download from these third parties.</p>\r\n<h3>User Content</h3>\r\n<p>The Site may contains reviews, travel guides, or other forums in which you can post content. If you use such areas on the Site, you are solely responsible for the travel information and other content, including without limitation, any reviews, text, images, links, or videos that you upload, transmit, or share with My Fam Trip or others on or through the Site (collectively, the \"User Content\"), and you represent and warrant that you are not transmitting or sharing User Content that you do not have permission to share. We do not guarantee any confidentiality with respect to the User Content and you understand that the User Content may be publicly displayed.</p>\r\n<p>When you provide My Fam Trip with User Content, you own the content you create and share, and you also grant My Fam Trip a perpetual, transferable, irrevocable, sub-licensable, fully-paid, worldwide license to use, modify, reproduce, distribute, prepare derivative works of, publicly perform, and publicly display (in tangible form and electronically) all User Content or other content provided to My Fam Trip. My Fam Trip can use the User Content in any format, channel, platform, or region with the right to localize the content into other languages. If uploaded or submitted to My Fam Trip, you further give My Fam Trip permission and the right to use your name, image, likeness, or other personal attributes for the purposes described in these Terms.</p>\r\n<p>You authorize My Fam Trip to make copies as we deem necessary in order to facilitate the storage and assimilation of the User Content on the Site. By providing My Fam Trip User Content, you represent and warrant that the User Content you provide will not violate or in any way infringe upon the rights of third parties, including property, contractual, employment, trade secrets, proprietary information, and nondisclosure rights, or any intellectual property rights. You may remove your User Content from the Site, but the license that you have granted will remain in effect. You understand that My Fam Trip does not control nor is My Fam Trip responsible for reviewing User Content. However, My Fam Trip reserves the right to review, edit, or delete any User Content or your account at any time. My Fam Trip is not in any way responsible or liable for such User Content or the messaging contained in User Content.</p>\r\n<h3>General</h3>\r\n<p>To the extent permitted by law, the laws of the State of Ohio, United States of America, without regard to its conflict of laws rules, will govern these Terms, as well as your and our observance of them. If you take any legal action relating to your use of the Site, including these Terms, you agree to file such action only in the state and federal courts located in Akron, Ohio, USA; if you are a consumer, the law may allow you to also bring proceedings in the courts for the place where you are domiciled. In any such action or any action My Fam Trip may initiate, the prevailing party will be entitled to recover all legal expenses incurred in connection with the action, including but not limited to costs, both taxable and non-taxable, and reasonable attorney fees. To the extent permitted by law, you agree that any disputes, claims and causes of action arising out of or connected with the Site and/or these Terms, will be resolved individually, without resort to any form of class action.</p>\r\n<p>Please note that My Fam Trip assumes no responsibility for reviewing unsolicited ideas for My Fam Trip&rsquo;s business (like product or advertising ideas) and will not incur any liability as a result of any similarities between those ideas and materials that may appear in future My Fam Trip products or services.</p>\r\n<h3>Miscellaneous</h3>\r\n<p>These Terms operate to the fullest extent permissible by law. If any provision of these Terms is unlawful, void or unenforceable, that provision is deemed severable from these Terms and does not affect the validity and enforceability of any remaining provisions.</p>\r\n<p>These Terms constitute a binding agreement between you and My Fam Trip and are accepted by you upon your use of the Site or your account. These Terms constitute the entire agreement between you and My Fam Trip regarding the use of the Site and your account.</p>\r\n<p>My Fam Trip may change the Site and these Terms at any time, in My Fam Trip&rsquo;s sole discretion and without notice to you. You are responsible for remaining knowledgeable about these Terms. Your continued use of the Site constitutes your acceptance of any changes to these Terms and any changes will supersede all previous versions of the Terms. Unless otherwise specified herein, all changes to these Terms apply to all users, including those enrolled before the date the changes take effect. Nothing contained in these Terms will be deemed to constitute either party as the agent or representative of the other party, or both parties as joint venturers or partners for any purpose. You may not assign, delegate or transfer your rights or obligations under these Terms. My Fam Trip may assign its rights and duties under these Terms without such assignment being considered a change to the Terms and without notice to you, provided your rights under these Terms are not prejudiced.</p>', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1567420952, 1567421838, 1, 1, 1, 1, NULL, 'basic', 'terms-conditions');

-- --------------------------------------------------------

--
-- Table structure for table `node_image`
--

CREATE TABLE `node_image` (
  `id` int(11) NOT NULL,
  `nid` int(11) DEFAULT NULL,
  `node_image` varchar(255) DEFAULT NULL,
  `node_image_title` varchar(255) DEFAULT NULL,
  `node_image_description` text,
  `default` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_image`
--

INSERT INTO `node_image` (`id`, `nid`, `node_image`, `node_image_title`, `node_image_description`, `default`) VALUES
(3, 3, 'image1-1565287006.png', 'image1', 'image1', 0),
(4, 3, 'image2-1565360206.png', 'image2', 'image2', 0),
(5, 3, 'image3-1565360207.png', 'image3', 'image3', 0);

-- --------------------------------------------------------

--
-- Table structure for table `node_sub`
--

CREATE TABLE `node_sub` (
  `id` int(11) NOT NULL,
  `nid` int(11) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `icon` int(11) DEFAULT NULL,
  `sub_description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `node_sub`
--

INSERT INTO `node_sub` (`id`, `nid`, `sub_title`, `icon`, `sub_description`) VALUES
(5, 7, 'Mission', 12, '<p>Our mission is to provide tools for travel professionals to maximize and enhance their Fam experiences.</p>'),
(6, 7, 'LOCATION', 13, '<p>My Fam Trip is headquartered in the Good ol&rsquo; Midwest USA with offices in Massillon, OH and Minneapolis, MN (where our affiliation to certain sports teams and love of travel thrives.) Don&rsquo;t get us started on the Cleveland Indians vs. Minnesota Twins topic.., you&rsquo;re just stirring the pot! :)</p>');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(510) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `module`, `name`, `value`) VALUES
(1, 'Image Upload Location', 'site', 'path', '../uploads'),
(2, 'Image Upload Url', 'site', 'url', 'http://opesmount.in/mft/uploads/'),
(3, 'Site name', 'base', 'name', 'MFT'),
(4, 'CSS file location', 'site', 'css', '../webassets/css/'),
(5, 'JS file location', 'site', 'js', '../webassets/js/'),
(6, 'Template Path', 'site', 'template', '@frontend/views/template/'),
(7, 'Layout Path', 'site', 'layout', '@frontend/views/layouts/'),
(8, 'Widget Path', 'site', 'widget', '@frontend/views/widgets/'),
(9, 'Block path', 'site', 'block', '@frontend/views/blocks/'),
(10, 'SMTP Host', 'smtp', 'host', 'mail.opesmount.in'),
(11, 'SMTP Username', 'smtp', 'username', 'orenda@opesmount.in'),
(12, 'SMTP Password', 'smtp', 'password', '1q2w3e4r'),
(13, 'SMTP Port', 'smtp', 'port', '587'),
(14, 'SMTP Encryption', 'smtp', 'encryption', ''),
(15, 'Facebook', 'custom', 'facebook', 'https://www.facebook.com/groups/1880724808909013'),
(16, 'Twitter', 'custom', 'twitter', ''),
(17, 'Instagram', 'custom', 'instagram', ''),
(18, 'Youtube Link', 'custom', 'youtube-link', 'https://youtu.be/8042wzYZ5fY'),
(19, 'Android Link', 'custom', 'android-link', ''),
(20, 'Ios Link', 'custom', 'ios-link', ''),
(21, 'Logo Header', 'img', 'logo-header', '10'),
(22, 'Logo Body', 'img', 'logo-body', '9'),
(23, 'Youtube Image', 'img', 'youtube-image', '11'),
(24, 'Google Map Key', 'custom', 'google-map-key', 'AIzaSyDNlOALy3golRPLA8Fmwz25FuL-kmPEF-Q'),
(25, 'SMTP Email', 'smtp', 'email', 'orenda@opesmount.in'),
(26, 'email', 'custom', 'email', 'hello@myfamtrip.com'),
(27, 'Captcha Site Key', 'custom', 'captcha-site-key', '6Le47rIUAAAAAA2tO0tFhSh2SbZNuTS42S9DFQY9'),
(28, 'Captcha Secret Key', 'custom', 'captcha-secret-key', '6Le47rIUAAAAAHgtd710BtlkgTQqmJlSnxJCo_Nj'),
(29, 'Banner Text', 'base', 'banner', '<h2>Everything Fam in the Palm of Your Hand!</h2>\r\n<h3>An app for travel professionals to maximize and enhance their Fam experiences.</h3>\r\n<ul>\r\n<li>One Place to Record it All</li>\r\n<li>Moneyshot Reminders</li>\r\n</ul>\r\n<ul>\r\n<li>Auto Watermarked Photos</li>\r\n<li>Market on the Fly</li>\r\n</ul>\r\n<ul>\r\n<li>Downloadable Reports</li>\r\n<li>Marketing &amp; Fam Tips</li>\r\n</ul>\r\n<ul>\r\n<li>Fam Etiquette My Fam Trip</li>\r\n<li>Go. See. Sell.</li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Table structure for table `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, '*', 'header_banner_title'),
(3, '*', 'header_banner_description'),
(4, '*', 'header_how_it'),
(5, '*', 'feature_title'),
(6, '*', 'feature_summary'),
(7, '*', 'what_people'),
(8, '*', 'footer_copyright'),
(9, '*', 'contact_sub_title'),
(10, '*', 'contact_note'),
(11, '*', 'contact_email_success'),
(12, '*', 'contact_email_error');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `style` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `style`, `type`, `file`) VALUES
(1, 'list', 'basic', 'listing-page.php'),
(2, 'list', 'service', 'listing-page.php'),
(3, 'list', 'package', 'package-listing.php'),
(4, 'detail', 'package', 'details-page.php'),
(5, 'detail', 'package', 'package-details.php'),
(6, 'detail', 'category', 'details-page.php'),
(7, 'detail', 'service', 'details-page.php'),
(8, 'detail', 'advanced', 'details-page.php'),
(9, 'detail', 'basic', 'details-page.php'),
(10, 'list', 'advanced', 'listing-page.php');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'W_4ZWWcpQZQ-wc5LuLwQ6UPasB8VZs_U', '$2y$13$pL7EZ3LR7ghLcC/Vb8UDAufOOlwefgRqPDRGKceKs/05SIdpxVwTG', NULL, 'admin@admin.com', 10, 1564485595, 1564485595, '0C7B0Hk63uCB-27o6T9_sP9PgZfvq75G_1564485595');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_image`
--
ALTER TABLE `block_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`b_id`);

--
-- Indexes for table `ImageManager`
--
ALTER TABLE `ImageManager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`),
  ADD KEY `idx_message_language` (`language`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `node`
--
ALTER TABLE `node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `node_image`
--
ALTER TABLE `node_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_id` (`nid`);

--
-- Indexes for table `node_sub`
--
ALTER TABLE `node_sub`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_node` (`nid`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_source_message_category` (`category`);

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `block_image`
--
ALTER TABLE `block_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ImageManager`
--
ALTER TABLE `ImageManager`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `node`
--
ALTER TABLE `node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `node_image`
--
ALTER TABLE `node_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `node_sub`
--
ALTER TABLE `node_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `block_image`
--
ALTER TABLE `block_image`
  ADD CONSTRAINT `fk_block_images` FOREIGN KEY (`b_id`) REFERENCES `block` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_source_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `node_image`
--
ALTER TABLE `node_image`
  ADD CONSTRAINT `fk_node_image` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `node_sub`
--
ALTER TABLE `node_sub`
  ADD CONSTRAINT `fk_sub_node` FOREIGN KEY (`nid`) REFERENCES `node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
