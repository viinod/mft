<?php
namespace common\base;

use Yii;
use yii\base\BootstrapInterface;

/*
/* The base class that you use to retrieve the settings from the database
*/

class Settings implements BootstrapInterface {

    private $db;

    public function __construct() {
        $this->db = Yii::$app->db;
    }

    /**
    * Bootstrap method to be called during application bootstrap stage.
    * Loads all the settings into the Yii::$app->params array
    * @param Application $app the application currently running
    */

    public function bootstrap($app) {

        // Get settings from database
        $sql = $this->db->createCommand("SELECT name,value,module FROM setting");
        $settings = $sql->queryAll();

        // Now let's load the settings into the global params array

        foreach ($settings as $key => $val) {
            Yii::$app->params['settings'][$val['module']][$val['name']] = $val['value'];
        }

        $sql1 = $this->db->createCommand("SELECT style,type,file FROM template");
        $settings1 = $sql1->queryAll();

        foreach ($settings1 as $key => $val) {
            Yii::$app->params['template'][$val['style']][$val['type']] = $val['file'];
        }
Yii::$app->set('mailer',[
                'class' => 'yii\swiftmailer\Mailer',
                'viewPath' => '@common/mail',
                'useFileTransport'=>false,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => Yii::$app->params['settings']['smtp']['host'],
                    'username' => Yii::$app->params['settings']['smtp']['username'],
                    'password' => Yii::$app->params['settings']['smtp']['password'],
                    'port' => Yii::$app->params['settings']['smtp']['port'],
                    'encryption' => Yii::$app->params['settings']['smtp']['encryption'],
                ],
            ]);

    }

}