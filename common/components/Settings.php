<?php

namespace common\components;

use Yii;
use yii\base\Component;
class Settings extends Component
{         
        public function custom($name)
        {
           
            return isset(Yii::$app->params['settings']['custom'][$name]) ? Yii::$app->params['settings']['custom'][$name] : NULL;
        }
        public function base($name)
        {
           
            return isset(Yii::$app->params['settings']['base'][$name]) ? Yii::$app->params['settings']['base'][$name] : NULL;
        }
        public function img($name)
        {
           
            return isset(Yii::$app->params['settings']['img'][$name]) ? Yii::$app->params['settings']['img'][$name] : NULL;
        }
}
?>
