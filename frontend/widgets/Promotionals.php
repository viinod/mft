<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use backend\models\Advanced;

class Promotionals extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = Advanced::find()->where(['type' => 'advanced','id' => 3])->one();
        }
        public function run()
        {
    	   return $this->render('widget--promotional', [
            'model' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['settings']['site']['widgets']) ? Yii::getAlias(Yii::$app->params['settings']['site']['widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
