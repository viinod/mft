<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;

class Featured extends Widget implements \yii\base\ViewContextInterface
{         
public $model;
        public function init()
        {
        	parent::init();
            $this->model = \backend\models\Block::find()->where(['status' => 1])->all();
        }
        public function run()
        {
    	   return $this->render('widget--featured', [
            'models' => $this->model,
        ]);

        }
        public function getViewPath()
        {
            return isset(Yii::$app->params['settings']['site']['widgets']) ? Yii::getAlias(Yii::$app->params['settings']['site']['widgets']) : Yii::getAlias('@frontend/views/widgets/');
        }
}
?>
