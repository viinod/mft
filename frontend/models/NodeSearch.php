<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Node;

/**
 * NodeSearch represents the model behind the search form about `backend\models\Node`.
 */
class NodeSearch extends Node
{
    /**
     * @inheritdoc
     */
    public $price_from;
    public $price_to;
    public function rules()
    {
        return [
            [['id', 'setting', 'started_at', 'ended_at', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status', 'base', 'menu'], 'integer'],
            [['title', 'summary', 'description', 'other', 'image', 'video', 'link', 'file', 'field1', 'field2', 'field3', 'field4', 'field5', 'text1', 'text2', 'text3', 'text4', 'text5', 'type', 'slug'], 'safe'],
            [['price', 'price_from', 'price_to', 'digit1', 'digit2', 'digit3', 'digit4', 'digit5'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$type)
    {
        $query = Node::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'price' => $this->price,
        //     'setting' => $this->setting,
        //     'digit1' => $this->digit1,
        //     'digit2' => $this->digit2,
        //     'digit3' => $this->digit3,
        //     'digit4' => $this->digit4,
        //     'digit5' => $this->digit5,
        //     'started_at' => $this->started_at,
        //     'ended_at' => $this->ended_at,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        //     'created_by' => $this->created_by,
        //     'updated_by' => $this->updated_by,
        //     'status' => $this->status,
        //     'base' => $this->base,
        //     'menu' => $this->menu,
        // ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['>=', 'price', $this->price_from])
            ->andFilterWhere(['<=', 'price', $this->price_to])
            // ->andFilterWhere(['like', 'summary', $this->summary])
            // ->andFilterWhere(['like', 'description', $this->description])
            // ->andFilterWhere(['like', 'other', $this->other])
            // ->andFilterWhere(['like', 'image', $this->image])
            // ->andFilterWhere(['like', 'video', $this->video])
            // ->andFilterWhere(['like', 'link', $this->link])
            // ->andFilterWhere(['like', 'file', $this->file])
            // ->andFilterWhere(['like', 'field1', $this->field1])
            // ->andFilterWhere(['like', 'field2', $this->field2])
            // ->andFilterWhere(['like', 'field3', $this->field3])
            // ->andFilterWhere(['like', 'field4', $this->field4])
            // ->andFilterWhere(['like', 'field5', $this->field5])
            // ->andFilterWhere(['like', 'text1', $this->text1])
            // ->andFilterWhere(['like', 'text2', $this->text2])
            // ->andFilterWhere(['like', 'text3', $this->text3])
            // ->andFilterWhere(['like', 'text4', $this->text4])
            // ->andFilterWhere(['like', 'text5', $this->text5])
            // ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'type', $type]);
            // ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}
