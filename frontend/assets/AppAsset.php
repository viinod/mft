<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@webroot/webassets';
    // public $css = [
    //     'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css',
    //     'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.4.1/css/all.min.css',
    //     'https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800,900,Poppins:100,300,400,700,800,900,Roboto:400,500,700,900&display=swap',
    //     'css/style.css',
    //     'css/page-efx.css',
    //     'css/owl.carousel.min.css',
    //     'css/responsive.css',

    // ];
    // public $js = [
    //     'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
    //     'https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js',
    //     'js/owl.carousel.js',
    //     'js/main.js',
    //     'js/modernizr.js',
    // ];
    // public $depends = [
    //     'yii\web\YiiAsset',
    //     'yii\bootstrap4\BootstrapAsset',
    //     'yii\bootstrap4\BootstrapPluginAsset'
    // ];
}
