<?php
use yii\helpers\Html;
?>
<section class="testimonial-area" data-scrollreveal="enter bottom over 2s after 0.5s">
<div class="container custome-width">
<div class="row">
<div class="col-lg-2 col-sm-12 testimonial-title">
<h3><?=Yii::t('*', 'what_people')?></h3>
</div>
<div class="col-lg-10 col-sm-12 testimonial-slider">
<div class="col-sm-12 owl-1 owl-slides no-padding">
<?php foreach ($models as $model):?>  
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="<?=Yii::getAlias('@url').'/testimonials/'.$model->image?>" alt="profile" class="rounded-circle">
<h4><?=$model->title?></h4>
<h6><?=$model->des?></h6>
<p><?=\yii\helpers\StringHelper::truncate($model->description,120,'...','UTF-8',false)?>
</p>
</div>
</div>
</div>
<?php endforeach;?>
</div>
</div>
</div>
</div>
</section>

               

              

            
