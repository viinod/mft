<?php
$this->title = 'My Farm Trip | About Us';
$this->registerMetaTag(['name' => 'description', 'content' => '']);
use yii\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
?>
<?php $this->beginBlock('header'); ?>
<div class="container inner-header1">
<div class="row">
<div class="col-sm-7 header-description" data-scrollreveal="enter left over 2s after 0.5s">
<h2><?=$model->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
</div>
</div>
<?php $this->endBlock(); ?>
<section class="features-section about-section">
<div class="container">
<div class="row">
<div class="col-sm-12 features-head">
<img src="<?=$directoryAsset?>/images/head-dots.png" alt="dots">
<p><?=$model->summary?></p>
<h3><?=$model->title?></h3>
</div>
<div class="col-sm-12 about-text about-main">
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('logo-body'),275,102)?>" alt="logo" data-scrollreveal="enter top over 2s after 0.5s">
<?=$model->description?>
</div>
<?php foreach ($model->nodeSubs as $sub):?>
<div class="col-sm-6 mission-vision-area" data-scrollreveal="enter left over 2s after 0.5s">
<img src="<?=Yii::$app->imagemanager->getImagePath($sub->icon)?>">
<h2><?=$sub->sub_title?></h2>
<?=$sub->sub_description?>
</div>
<?php endforeach; ?>
</div>
</div>
</section>