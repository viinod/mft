<?php
$this->title = 'My Farm Trip | Home';
$this->registerMetaTag(['name' => 'description', 'content' => '']);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
use yii\helpers\Html;
?>
<?php $this->beginBlock('header'); ?>
<div class="container">
<div class="row">
<div class="col-lg-7 col-sm-12 header-description header-points" data-scrollreveal="enter left over 2s after 0.2s">
<?=Yii::$app->settings->base('banner')?>
<div class="row justify-content-start">
<div class="col-lg-7 col-sm-12 header-video">
<h2><?=Yii::t('*', 'header_how_it')?></h2>
<a href="<?=Yii::$app->settings->custom('youtube-link')?>" data-lity>
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('youtube-image'),600,321)?>" alt="youtube">
</a>
</div>
</div>
</div>
<?=\frontend\widgets\Slider::widget()?>
</div>
</div>
<?php $this->endBlock(); ?>
<section class="features-section">
<div class="circle-bg">
<div class="container">
<div class="row">
<div class="col-sm-12 features-head">
<img src="<?=$directoryAsset ?>/images/head-dots.png" alt="dots">
<h3><?=Yii::t('*', 'feature_title')?></h3>
</div>
</div>
</div>
</div>
<div class="features-bg">
<div class="container">
<div class="row">
<?=\frontend\widgets\Block::widget(['id' => 1])?>
<?=\frontend\widgets\Block::widget(['id' => 2])?>
<?=\frontend\widgets\Block::widget(['id' => 3])?>
<?=\frontend\widgets\Block::widget(['id' => 4])?>
<?=\frontend\widgets\Block::widget(['id' => 5])?>
<?=\frontend\widgets\Block::widget(['id' => 6])?>
</div>
</div>
</div>
</section>
<section class="app-splash-area">
<div class="container-fluid">
<div class="row">
<?=\frontend\widgets\Promotionals::widget()?>
<div class="col-sm-12 download-btn-area">
<a href="<?=Yii::$app->settings->custom('android-link')?>" class="btn btn-primary" data-scrollreveal="enter bottom over 2s after 0.5s"><i class="fab fa-android"></i> &nbsp; Download Here</a>
<a href="<?=Yii::$app->settings->custom('ios-link')?>" class="btn btn-secondary" data-scrollreveal="enter bottom over 2s after 0.5s"><i class="fab fa-apple"></i> &nbsp; Download Here</a>
</div>
</div>
</div>
</section>
<?php //echo \frontend\widgets\Testimonials::widget()?>
