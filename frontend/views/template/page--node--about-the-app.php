<?php
$this->title = 'My Farm Trip | About The App';
$this->registerMetaTag(['name' => 'description', 'content' => '']);
use yii\helpers\Url;
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
$first = true;
?>
<?php $this->beginBlock('header'); ?>
<div class="container inner-header">
<div class="row">
<div class="col-sm-7 header-description" data-scrollreveal="enter left over 2s after 0.5s">
<h2><?=$model->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
<div class="col-sm-5 header-video" data-scrollreveal="enter right over 2s after 0.5s">
<h2><?=Yii::t('*', 'header_how_it')?></h2>
<a href="<?=Yii::$app->settings->custom('youtube-link')?>" data-lity>
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('youtube-image'),600,321)?>" alt="youtube">
</a>
</div>
</div>
</div>
<?php $this->endBlock(); ?>
<section class="features-section about-section">
<div class="container">
<div class="row">
<div class="col-sm-12 features-head">
<img src="<?=$directoryAsset?>/images/head-dots.png" alt="dots">
<p><?=$model->summary?></p>
<h3><?=$model->title?></h3>
</div>
<div class="col-sm-12 mobile-splash-screens" data-scrollreveal="enter top over 2s after 0.5s">
<div class="mobile-slider2">
<div id="carouselExampleIndicators2" class="carousel slide carousel-fade" data-ride="carousel">
<div class="carousel-inner d-none d-xl-block d-md-block d-lg-block">
<?php foreach ($model->nodeImages as $image):?>
<?php if($first): $first = false; ?>	
<div class="carousel-item active">
<img src="<?=Yii::getAlias('@url/nodeimages/').$image->node_image?>" alt="<?=$image->node_image_title?>">
</div>
<?php else: ?>
<div class="carousel-item">
<img src="<?=Yii::getAlias('@url/nodeimages/').$image->node_image?>" alt="<?=$image->node_image_title?>">
</div>
<?php endif; ?>
<?php endforeach; ?>
</div>
<div class="carousel-inner d-md-none d-lg-none d-sm-block">
<div class="col-sm-12 carousel-item active">
<img src="<?=Yii::$app->imagemanager->getImagePath(7,354,453)?>" alt="slides">
</div>
<div class="col-sm-12 carousel-item">
<img src="<?=Yii::$app->imagemanager->getImagePath(8,354,453)?>" alt="slides">
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-12 about-text">
<?=$model->description?>
</div>
</div>
</div>
</section>
<section class="tabing-area">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 features-head">
<img src="<?=$directoryAsset?>/images/head-dots.png" alt="dots">
<h3><?=Yii::t('*', 'feature_title')?></h3>
</div>
</div>
<?=\frontend\widgets\Featured::widget()?>
</div>
</section>