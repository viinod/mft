<?php
$this->title = 'My Farm Trip | Contact';
$this->registerMetaTag(['name' => 'description', 'content' => '']);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
use yii\helpers\Html;
use common\helpers\Url;
$this->title = $model->title;
$this->context->layout = 'contact';
?>
<?php $this->beginBlock('header'); ?>
<div class="container inner-header">
<div class="row">
<div class="col-sm-7 header-description" data-scrollreveal="enter left over 2s after 0.5s">
<h2><?=$model->title?></h2>
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Url::home()?>"><i class="fas fa-home"></i></a></li>
<li class="list-inline-item seperation">/</li>
<li class="list-inline-item"><?=$model->title?></li>
</ul>
</div>
<div class="col-sm-5 header-video" data-scrollreveal="enter right over 2s after 0.5s">
<h2><?=Yii::t('*', 'header_how_it')?></h2>
<a href="<?=Yii::$app->settings->custom('youtube-link')?>" data-lity>
<img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('youtube-image'),600,321)?>" alt="youtube">
</a>
</div>
</div>
</div>
<?php $this->endBlock(); ?>
<section class="Address-area">
<div class="container">
<div class="row contact-info-area">
<div class="col-sm-12 contact-info" data-scrollreveal="enter top over 2s after 1.5s">
<div class="circle-icon">
<i class="fas fa-envelope-open"></i>
</div>
<h3>Mail</h3>
<p>
<?=Yii::$app->settings->custom('email')?>
</p>
</div>
</div>
</div>
</section>
<section class="features-section about-section">
<div class="container-fluid">
<div class="row justify-content-md-center">
<div class="col-sm-12 features-head contact-head">
<img src="<?=$directoryAsset?>/images/head-dots.png" alt="dots">
<h3><?=$model->title?></h3>
<p><?=Yii::t('*', 'contact_sub_title')?></p>
</div>
<div class="col-lg-8 col-sm-12 contact-form-area">
<div class="alert alert-success alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'contact_email_success')?>
</div>
<div class="alert alert-danger alert-dismissable collapse">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<?=Yii::t('*', 'contact_email_error')?>
</div>
<?= Html::beginForm(['site/mail'],'post',['id' => 'cform']) ?>
<div class="row">	
<div class="col-lg-6 col-sm-12 contact-form" data-scrollreveal="enter left over 2s after 0.5s">
<div class="form-group">
<input type="text" name="name" id="cname" class="form-control" required>
<label class="form-control-placeholder" for="name">Name</label>
</div>
<div class="form-group">
<input type="email" name="email" id="cemail" class="form-control" required>
<label class="form-control-placeholder" for="email">Email</label>
</div>
<div class="form-group">
<input type="text" name="subject" id="csubject" class="form-control" required>
<label class="form-control-placeholder" for="text">Subject</label>
</div>
</div>
<div class="col-lg-6 col-sm-12 contact-form" data-scrollreveal="enter right over 2s after 0.5s">
<div class="form-group">
<textarea class="form-control text-area" rows="5" name="message" id="cmessage" required></textarea>
<label class="form-control-placeholder" for="email">Message</label>
</div>
</div>
<div class="col-sm-6 indication-text" data-scrollreveal="enter top over 2s after 0.5s">
<?=Yii::t('*', 'contact_note')?>
</div>
<div class="col-sm-6 submit-area" data-scrollreveal="enter top over 2s after 0.5s">
<div class="form-group">
<div id='loadingmessage' style='display:none'>
<img src='<?=$directoryAsset?>/images/ajax-loader.gif'/>
</div>
<button type="submit" class="btn btn-primary btn-lg" name="contact-button" id="contact-button">Submit</button>
</div>
</div></div>
<?= Html::endForm() ?>
</div>
<div class="col-lg-12 col-sm-12 map-area no-padding">
<div id="googleMap" style="width:100%;height:100%"></div>
</div>
</div>
</div>
</section>