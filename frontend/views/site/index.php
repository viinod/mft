<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<section class="section-overlay">
</section>
<section class="features-section">
<div class="circle-bg">
<div class="container">
<div class="row">
<div class="col-sm-12 features-head">
<img src="images/head-dots.png" alt="dots">
<p>Lorem ipsum dolor sit amet consectetur </p>
<h3>App Features</h3>
</div>
</div>
</div>
</div>
<div class="features-bg">
<div class="container">
<div class="row">
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.1s">
<div class="features color-1">
<img src="images/features1.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.2s">
<div class="features color-2">
<img src="images/features2.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.3s">
<div class="features color-3">
<img src="images/features3.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.4s">
<div class="features color-4">
<img src="images/features4.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.5s">
<div class="features color-5">
<img src="images/features5.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
<div class="col-lg-4 col-sm-6" data-scrollreveal="enter top over 2s after 0.6s">
<div class="features color-6">
<img src="images/features6.png" alt="icon">
<h3>Device Compatibility</h3>
<div class="custom-css" style="width:100%">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore </p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<section class="app-splash-area">
<div class="container-fluid">
<div class="row">
<div class="col-sm-12 app-screen-head">
<img class="dots-head" src="images/head-dots.png" alt="dots">
<img src="images/logo.png" alt="logo">
<p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt labore et dolore magna Consectetur adipiscing elit, sed do ei</p>
</div>
<div class="col-sm-12 mobile-splash-screens" data-scrollreveal="enter top over 2s after 0.5s">
<div class="mobile-slider2">
<div id="carouselExampleIndicators2" class="carousel slide carousel-fade" data-ride="carousel">
<div class="carousel-inner d-none d-xl-block d-md-block d-lg-block">
<div class="carousel-item active">
<img src="images/mobile-splash.png" alt="slides">
</div>
<div class="carousel-item">
<img src="images/mobile-splash2.png" alt="slides">
</div>
<div class="carousel-item">
<img src="images/mobile-splash3.png" alt="slides">
</div>
</div>
<div class="carousel-inner d-md-none d-lg-none d-sm-block">
<div class="col-sm-12 carousel-item active">
<img src="images/device-xs-1.png" alt="slides">
</div>
<div class="col-sm-12 carousel-item">
<img src="images/device-xs-2.png" alt="slides">
</div>
</div>
</div>
</div>
</div>
<div class="col-sm-12 download-btn-area">
<button type="button" class="btn btn-primary" data-scrollreveal="enter bottom over 2s after 0.5s"><i class="fab fa-android"></i> &nbsp; Download Here</button>
<button type="button" class="btn btn-secondary" data-scrollreveal="enter bottom over 2s after 0.5s"><i class="fab fa-apple"></i> &nbsp; Download Here</button>
</div>
</div>
</div>
</section>
<section class="testimonial-area" data-scrollreveal="enter bottom over 2s after 0.5s">
<div class="container custome-width">
<div class="row">
<div class="col-lg-2 col-sm-12 testimonial-title">
<h3>What
People
are
saying?</h3>
</div>
<div class="col-lg-10 col-sm-12 testimonial-slider">
<div class="col-sm-12 owl-1 owl-slides no-padding">
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
<div class="item">
<div class="row">
<div class="col-sm-12 profile">
<img src="images/profile2.jpg" alt="profile" class="rounded-circle">
<h4>John Doe</h4>
<h6>CEO @ Loremipsum</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>