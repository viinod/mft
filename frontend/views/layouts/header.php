<?php
use common\helpers\Url;
?>
<header class="header-bg">
<div class="container-fluid">
<div class="row">
<div class="col-lg-4 col-sm-5 logo">
<a href="<?=Url::home()?>"><img src="<?=Yii::$app->imagemanager->getImagePath(Yii::$app->settings->img('logo-header'),199,64)?>" alt="logo"></a>
</div>
<div class="col-lg-8 col-sm-7 menu">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item active">
<a href="<?=Url::home()?>" class="nav-link">Home<span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a href="<?=Url::node('/node/3')?>" class="nav-link">About the App</a>
</li>
<li class="nav-item">
<a href="<?=Url::node('/node/4')?>" class="nav-link">Contact</a>
</li>
<li class="nav-item">
<a href="<?=Url::node('/node/12')?>" class="nav-link"> About Us </a>
</li>
</ul>
</div>
</nav>
</div>
</div>
</div>
<?php
if(isset($this->blocks['header'])){
  echo $this->blocks['header'];
}
?>
</header>
<section class="section-overlay">
</section>