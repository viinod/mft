<?php
use common\helpers\Url;
?>
<footer class="footer-bg">
<div class="container">
<div class="row">
<div class="col-lg-8 col-sm-12 copyright-text">
<p><?=Yii::t('*', 'footer_copyright', time())?> | <a href="<?=Url::node('/node/5')?>">Privacy Policy</a> | <a href="<?=Url::node('/node/5')?>">Terms & Conditions</a></p>
</div>
<div class="col-lg-4 col-sm-12 footer-social">
<div class="row">
<div class="col-lg-11 col-sm-6 footer-download-btn">
<a href="<?=Yii::$app->settings->custom('android-link')?>" class="btn btn-primary"><i class="fab fa-android"></i> &nbsp; Download Here</a>
<a href="<?=Yii::$app->settings->custom('ios-link')?>" class="btn btn-secondary"><i class="fab fa-apple"></i> &nbsp; Download Here</a>
</div>
<div class="col-lg-1 col-sm-6 footer-connect">
<ul class="list-inline">
<li class="list-inline-item"><a href="<?=Yii::$app->settings->custom('facebook')?>" class="footer-icons"><i class="fab fa-facebook-f"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</footer>