<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
AppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= Html::encode($this->title) ?></title>
<?php $this->registerCsrfMetaTags() ?>
<?php $this->head() ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/style.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800,900,Poppins:100,300,400,700,800,900,Roboto:400,500,700,900&display=swap">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/page-efx.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/responsive.css">
<script src="https://www.google.com/recaptcha/api.js?render=<?=Yii::$app->settings->custom('captcha-site-key')?>"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('<?=Yii::$app->settings->custom('captcha-site-key')?>', { action: 'contact' }).then(function (token) {
              $('#cform').prepend('<input type="hidden" name="recaptcha" value="' + token + '">');
            });
        });
    </script>
</head>
<body>
<?php $this->beginBody() ?>
<?php $this->beginBody() ?>
<?= $this->render(
    'header.php',
    ['directoryAsset' => $directoryAsset]
) ?>
        <?= $content ?>
<?= $this->render(
    'footer.php',
    ['directoryAsset' => $directoryAsset]
) ?>

<?php $this->endBody() ?>
 <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="<?=$directoryAsset ?>/js/main.js"></script>
    <script src="<?=$directoryAsset ?>/js/modernizr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
<script>
function myMap() {
var mapProp= {
  center:new google.maps.LatLng(10.002591, 76.356860),
  zoom:13,
};
var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=Yii::$app->settings->custom('google-map-key')?>&callback=myMap"></script>
     <script>
      $('#cform').submit( function(e) {
        e.preventDefault();
        var data = $(this).serializeArray();
        var url = $(this).attr('action');                 
                $('#contact-button').hide();
                 $('#loadingmessage').show();
                $.ajax({
                  type: 'POST',
                  url: url,
                  data: data,
                  success: function( response ) {
                            if(response.success) {
                                $(".alert-success").show();

                            } else {
                                   $(".alert-danger").show();
                            }
                      $("#cform")[0].reset();
                      $('#loadingmessage').hide();
                     $('#contact-button').show();

                  }
              });
          });
    </script>
</body>
</html>
<?php $this->endPage() ?>
