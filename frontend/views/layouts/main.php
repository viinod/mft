<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
AppAsset::register($this);
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@webroot/webassets');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= Html::encode($this->title) ?></title>
<?php $this->registerCsrfMetaTags() ?>
<?php $this->head() ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/style.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700,800,900,Poppins:100,300,400,700,800,900,Roboto:400,500,700,900&display=swap">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/page-efx.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/owl.carousel.min.css">
<link rel="stylesheet" href="<?=$directoryAsset ?>/css/responsive.css">
</head>
<body>
<?php $this->beginBody() ?>
<?php $this->beginBody() ?>
<?= $this->render(
    'header.php',
    ['directoryAsset' => $directoryAsset]
) ?>
        <?= $content ?>
<?= $this->render(
    'footer.php',
    ['directoryAsset' => $directoryAsset]
) ?>

<?php $this->endBody() ?>
 <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="<?=$directoryAsset ?>/js/main.js"></script>
    <script src="<?=$directoryAsset ?>/js/modernizr.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lity/2.3.1/lity.min.js"></script>
    <script src="<?=$directoryAsset ?>/js/owl.carousel.js"></script>
    <script src="<?=$directoryAsset ?>/js/jquery.show-more.js"></script>
    <script>
$(".owl-1").owlCarousel({loop:true,margin:40,responsiveClass:true,dots:true,responsive:{0:{items:1,nav:false},600:{items:2,nav:false,},768:{items:2,nav:false,},1000:{items:4,nav:false,loop:true,autoplay:true,dots:true,autoplayTimeout:5000}}});$(".custom-css").showMore({minheight:80,buttontxtmore:'Read More <i class="fas fa-angle-double-right"></i>',buttontxtless:'Read Less <i class="fas fa-angle-double-right"></i>',buttoncss:"my-button-css",animationspeed:250});
        </script>
</body>
</html>
<?php $this->endPage() ?>
