<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "node_sub".
 *
 * @property int $id
 * @property int $nid
 * @property string $sub_title
 * @property int $icon
 * @property string $sub_description
 *
 * @property Node $n
 */
class NodeSub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'node_sub';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_title'], 'required'],
            [['nid', 'icon'], 'integer'],
            [['sub_description'], 'string'],
            [['sub_title'], 'string', 'max' => 255],
            [['nid'], 'exist', 'skipOnError' => true, 'targetClass' => Node::className(), 'targetAttribute' => ['nid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nid' => 'Nid',
            'sub_title' => 'Title',
            'icon' => 'Icon',
            'sub_description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(Node::className(), ['id' => 'nid']);
    }
}
