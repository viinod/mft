<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $style
 * @property string $type
 * @property string $file
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $name;
    public $category;
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['type', 'file'], 'required','on' => 'assigning-template'],
            [['style', 'type', 'file'], 'string', 'max' => 255],
            [['type', 'file'], 'unique', 'targetAttribute' => ['type', 'file'], 'message' => 'Already added this combination'],
            [['name','category','style'], 'required','on' => 'file-create'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'style' => 'Style',
            'type' => 'Type',
            'file' => 'File',
        ];
    }
}
