<?php

namespace backend\controllers;

use Yii;
use backend\models\Advanced;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Menu;
use backend\models\Seo;
use backend\models\Bannermanager;
use yii\web\UploadedFile;
use backend\models\Model;
use backend\models\NodeImage;
use backend\models\NodeSub;
use yii\helpers\ArrayHelper;

/**
 * AdvancedController implements the CRUD actions for Advanced model.
 */
class AdvancedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
                        'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Advanced models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Advanced::find()->where(['type' => 'advanced']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advanced model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionSee($id,$nid)
    {
        $model = NodeSub::findOne(['id' => $id,'nid' => $nid]);
        return $this->render('see', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    /**
     * Creates a new Advanced model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

public function actionCreate()
    {
        $model = new Advanced();
        $images = [new NodeImage(['scenario' => 'create-only'])];

       if ($model->load(Yii::$app->request->post())) {

            $images = Model::createMultiple(NodeImage::classname());
            Model::loadMultiple($images, Yii::$app->request->post());
            foreach ($images as $index => $image) {
                $image->file = UploadedFile::getInstance($image, "[{$index}]file");
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($images) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();

                try {
            $model->type = 'advanced';
            $model->base = 1;
                    if ($flag = $model->save(false)) {

                        foreach ($images as $index => $image) {
                            $image->nid = $model->id;
                    $image->node_image = ($image->file != NULL) ? str_replace(' ', '-', $image->node_image_title).'-'.time().'.'.$image->file->extension : "";
                            if (! ($flag = $image->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                    foreach ($images as $image) {
                       $image->file->saveAs(Yii::getAlias('@path').'/nodeimages/'.$image->node_image); 
                    }
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
                'model' => $model,
            'images' => (empty($images)) ? [new NodeImage] : $images
        ]);
    }
    /**
     * Updates an existing Advanced model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $images = $model->nodeImages;

       if ($model->load(Yii::$app->request->post())) {

            $oldIDs = ArrayHelper::map($images, 'id', 'id');
            $images = Model::createMultiple(NodeImage::classname(),$images);
            Model::loadMultiple($images, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($images, 'id', 'id')));

            foreach ($images as $index => $image) {
                $image->file = UploadedFile::getInstance($image, "[{$index}]file");
            }

            // validate all models
            $valid = $model->validate();
            $valid = Model::validateMultiple($images) && $valid;

            if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();

                try {
            $model->type = 'advanced';
            if ($flag = $model->save(false)) {
                  if (!empty($deletedIDs)) {
                            NodeImage::deleteAll(['id' => $deletedIDs]);
                        }
                        foreach ($images as $image) {
                            $image->nid = $model->id;
                    $image->node_image = ($image->file != NULL) ? str_replace(' ', '-', $image->node_image_title).'-'.time().'.'.$image->file->extension : $image->node_image;
                            if (! ($flag = $image->save(false))) {
                                $transaction->rollBack();
                                break;
                            }
                        }
                    }

                    if ($flag) {
                    foreach ($images as $image) {
                        if($image->file != NULL){
                       $image->file->saveAs(Yii::getAlias('@path').'/nodeimages/'.$image->node_image); }
                    }
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } catch (Exception $e) {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
                'model' => $model,
            'images' => (empty($images)) ? [new NodeImage] : $images
            ]);
    }

    /**
     * Deletes an existing Advanced model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        NodeSub::deleteAll(['nid' => $id]);
        NodeImage::deleteAll(['nid' => $id]);
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['index']);
    }
    public function actionThrash($nid,$id)
    {
        if (($model = NodeSub::findOne(['id' => $id,'nid' => $nid])) !== null) {
            $model->delete();
        }
        return $this->redirect(['list', 'nid' => $nid]);
    }
    public function actionList($nid)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => NodeSub::find(),
        ]);
        $dataProvider->query->where(['nid' => $nid])->all();
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'node' => $this->findModel($nid),
        ]);
    }
    public function actionAdd($nid)
    {
        $model = new NodeSub();
        if ($model->load(Yii::$app->request->post())) {
            $model->nid = $nid;
            $model->save();
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('add', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
     public function actionEdit($nid,$id)
    {
        $model = NodeSub::findOne(['id' => $id,'nid' => $nid]);
        if ($model->load(Yii::$app->request->post())) {
            $model->nid = $nid;
            $model->save();
            return $this->redirect(['see', 'id' => $model->id,'nid' => $nid]);
        }
        return $this->render('edit', [
            'model' => $model,
            'node' => $this->findModel($nid),
        ]);
    }
    /**
     * Finds the Advanced model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advanced the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advanced::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
