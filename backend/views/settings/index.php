<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
$this->title = 'Base Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?php
$form = ActiveForm::begin();
 foreach ($settings as $index => $setting) {
 	if($setting->name == 'banner'){
  echo $form->field($setting, "[$index]value")->widget(TinyMce::className(), [
    'options' => ['rows' => 12],
    'language' => 'en_CA',
    'clientOptions' => [
        'plugins' => [
            "lists",
            "code",
            "contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist outdent"
    ]
])->label($setting->title);
 	}else{
 		echo $form->field($setting, "[$index]value")->label($setting->title);
 	}
            
 }
 ?>
<div class="form-group">
    <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();