<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Main Menu', 'options' => ['class' => 'header']],
                    [
                        'label' => 'Pages',
                        'icon' => 'file-text-o ',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Banners', 'icon' => 'circle-o', 'url' => ['/banner']],
                            ['label' => 'Basic Page', 'icon' => 'circle-o', 'url' => ['/basic']],
                            ['label' => 'Advanced Page', 'icon' => 'circle-o', 'url' => ['/advanced']],
                            ['label' => 'Testimonials', 'icon' => 'circle-o', 'url' => ['/testimonial']],

                        ],
                    ],
                    // [
                    //     'label' => 'Template',
                    //     'icon' => 'clone',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Create Files', 'icon' => 'circle-o', 'url' => ['/template'],],
                    //         ['label' => 'Template Editor', 'icon' => 'circle-o', 'url' => ['/template/php'],],
                    //         ['label' => 'Layout Editor', 'icon' => 'circle-o', 'url' => ['/template/layout'],],
                    //         ['label' => 'Widget Editor', 'icon' => 'circle-o', 'url' => ['/template/widget'],],
                    //         ['label' => 'Css Editor', 'icon' => 'circle-o', 'url' => ['/template/css'],],
                    //         ['label' => 'Js Editor', 'icon' => 'circle-o', 'url' => ['/template/js'],],
                    //     ],
                    // ],
                    ['label' => 'Blocks', 'icon' => 'th', 'url' => ['/block']],
                    [
                        'label' => 'Settings',
                        'icon' => 'gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Base Settings', 'icon' => 'circle-o', 'url' => ['/settings/index']],
                            ['label' => 'Custom Settings', 'icon' => 'circle-o', 'url' => ['/settings/custom']],
                            ['label' => 'SMTP Settings', 'icon' => 'circle-o', 'url' => ['/settings/smtp']],
                            ['label' => 'Image Settings', 'icon' => 'circle-o', 'url' => ['/settings/image']],
                            ['label' => 'Keywords', 'icon' => 'circle-o', 'url' => ['/keyword/index']],
                       ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
