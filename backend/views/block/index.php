<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blocks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?=  GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'title',
        //'category',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {view}',
        ],
    ],
]); ?>

</div>
