<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Article */

$this->title = $model->sub_title;;
$this->params['breadcrumbs'][] = ['label' => 'Advanced Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $node->title, 'url' => ['list', 'nid' => $node->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['edit', 'id' => $model->id, 'nid' => $node->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['thrash', 'id' => $model->id, 'nid' => $node->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sub_title',
            'sub_description:html',
            [
            'attribute'=>'icon',
            'value'=> \Yii::$app->imagemanager->getImagePath($model->icon),
            'format' => ['image',['width'=>'50']],
            ],
        ],
    ]) ?>

</div>
