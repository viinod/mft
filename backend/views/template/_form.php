<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-form">

    <?php $form = ActiveForm::begin(); ?>

<?= $form->field($model,'type')->dropDownList(
                                ArrayHelper::map(\backend\models\Page::find()->select(['type'])->groupBy(['type'])->asArray()->all(), 'type', 'type'),
                                [
                                    'prompt'=>'Select Content Type',
                                ]); ?>
<?= $form->field($model, 'style')->dropDownList([ 'list' => 'list', 'detail' => 'detail'],['prompt' => 'Please select']); ?>
<?= $form->field($model,'file')->dropDownList(
                                $templates,
                                [
                                    'prompt'=>'Select Content Type',
                                ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
