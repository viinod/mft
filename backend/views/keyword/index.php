<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Keywords';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Keyword', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
             'attribute' => 'id',
             'value' => function($data) {
                    return $data->id0->message;
                },                
            ],
            [
             'attribute' => 'translation',
             'value' => function($data) {
                    return \yii\helpers\StringHelper::truncate($data->translation,50,'...','UTF-8',false);
                },                
            ],

            [
                'class' => 'yii\grid\ActionColumn']
        ],
    ]); ?>


</div>
